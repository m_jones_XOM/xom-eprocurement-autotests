package com.xommaterials.eprocurement.autotest;

import com.xommaterials.eprocurment.autotest.skeleton.Base;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MainViewTest extends BaseTest {

    private static final Logger LOGGER = LogManager.getLogger(Base.class.getName());

    @Test(priority = 1)
    public void mainViewAccess() throws InterruptedException, IOException {
        
        login();

        Assert.assertTrue(mainView.getLogoutBtn().isDisplayed());
        Assert.assertTrue(mainView.getCopyright().isDisplayed());
        Assert.assertTrue(mainView.getLogoutBtn().isEnabled());

        LOGGER.info("All Webelements on MainView are displayed and active");
    }
}
