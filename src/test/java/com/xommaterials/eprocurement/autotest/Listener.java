package com.xommaterials.eprocurement.autotest;

import org.testng.ITestListener;
import com.xommaterials.eprocurment.autotest.skeleton.Base;
import org.testng.ITestResult;

import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Listener implements ITestListener {
    
    private static final Logger LOGGER = LogManager.getLogger(Listener.class);

    private final Base base = new Base();

    @Override
    public void onTestFailure(ITestResult iTestResult){
        iTestResult.getName();
        try {
            base.getScreenshot(iTestResult.getName());
        }catch (IOException e){
            LOGGER.error("an exception occured while getting screenshot...", e);
        }
    }
}
