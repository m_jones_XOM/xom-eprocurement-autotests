package com.xommaterials.eprocurement.autotest;

import com.xommaterials.eprocurment.autotest.skeleton.Base;
import com.xommaterials.eprocurment.autotest.skeleton.DataProviderClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class LandingPageTest extends BaseTest {

    private static final Logger LOGGER = LogManager.getLogger(Base.class.getName());

    @Test(priority = 1)
    public void landingPageAccess() throws IOException {
        driver.get(url);

        Assert.assertTrue(landingPage.getPageTitle().isDisplayed());
        Assert.assertTrue(landingPage.getInputName().isDisplayed());
        Assert.assertTrue(landingPage.getInputPW().isDisplayed());
        Assert.assertTrue(landingPage.getForgotPw().isDisplayed());
        Assert.assertTrue(landingPage.getRegBuyer().isDisplayed());
        Assert.assertTrue(landingPage.getSubmitBtn().isDisplayed());

        Assert.assertTrue(landingPage.getInputName().isEnabled());
        Assert.assertTrue(landingPage.getInputPW().isEnabled());
        Assert.assertTrue(landingPage.getForgotPw().isEnabled());
        Assert.assertTrue(landingPage.getRegBuyer().isEnabled());
        Assert.assertTrue(landingPage.getSubmitBtn().isEnabled());

        LOGGER.info("All Webelements on Landingpage are displayed and active");
    }

    @Test(priority = 2, dataProvider = "data-provider", dataProviderClass = DataProviderClass.class)
    public void landingPageLoginInvalid(String username, String password) throws InterruptedException {

        driver.get(url);

        Thread.sleep(2000);
        landingPage.getInputName().sendKeys(username);
        landingPage.getInputPW().sendKeys(password);

        landingPage.getSubmitBtn().click();
        Thread.sleep(2000);

        Assert.assertTrue(landingPage.getErrorMsg().isDisplayed());
        LOGGER.info("Error message is displayed");
    }

    @Test(priority = 3)
    public void landingPageLoginValid() throws IOException, InterruptedException {
        login();
    }
}
