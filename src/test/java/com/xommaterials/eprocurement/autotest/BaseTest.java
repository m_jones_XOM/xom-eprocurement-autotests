package com.xommaterials.eprocurement.autotest;

import com.xommaterials.eprocurment.autotest.skeleton.Base;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BaseTest extends Base {    
        
    private static final Logger LOGGER  = LogManager.getLogger(Base.class.getName());

    public void login() throws IOException, InterruptedException {

        driver.get(url);

        Thread.sleep(2000);

        landingPage.getInputName().click();
        landingPage.getInputName().clear();
        landingPage.getInputName().sendKeys(testuser);

        landingPage.getInputPW().click();
        landingPage.getInputPW().clear();
        landingPage.getInputPW().sendKeys(testuserpw);

        landingPage.getSubmitBtn().click();

        LOGGER.info("Logged in with valid data");
    }
}
