package com.xommaterials.eprocurment.autotest.skeleton;

import com.xommaterials.eprocurment.autotest.pageobejct.Landingpage;
import com.xommaterials.eprocurment.autotest.pageobejct.MainView;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class Base {

    private static final String BROWSER_NAME_CHROME = "chrome";
    private static final String BROWSER_NAME_FIREFOX = "FF";
    private static final String BROWSER_NAME_IE = "IE";
    private static final String BROWSER_NAME_EDGE = "Edge";

    private static final String OS_TYPE_WINDOWS = "windows";
    private static final String OS_TYPE_LINUX = "linux";
    
    private static final Logger LOGGER  = LogManager.getLogger(Base.class.getName());

    public static WebDriver driver;
    private final Properties prop;
    
    protected Landingpage landingPage = null;
    protected MainView mainView = null;
    protected final String url;
    protected final String testuser;
    protected final String testuserpw;

    
    @BeforeClass
    public void initialize() throws IOException {
        driver = initializeDriver();
        LOGGER.info("Driver is initialized");
        
        mainView = new MainView(driver);
        landingPage = new Landingpage(driver);
    }

    @AfterClass
    public void tearDown(){
        driver.quit();
    }


    public Base() {
        prop = new Properties();
        
        try {            
            final ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            final InputStream is = classloader.getResourceAsStream("data.properties");
            prop.load(is);
        } catch (IOException ex) {
           LOGGER.error("an exception occured...", ex);
        }

        url = prop.getProperty("url");
        testuser = prop.getProperty("testuser");
        testuserpw = prop.getProperty("testuserpw");
    }

    public WebDriver initializeDriver() {

        final String browserName = prop.getProperty("browser");
        final String basePath = prop.getProperty("basePath");
        final String osType = prop.getProperty("operatingSystem")!=null ? prop.getProperty("operatingSystem") : OS_TYPE_WINDOWS;

        switch (osType) {
            case OS_TYPE_LINUX:
                 switch (browserName) {
                    case BROWSER_NAME_CHROME:
                        System.setProperty("webdriver.chrome.driver", basePath + "chromedriver_Linux64");
                        driver = new ChromeDriver();
                        driver.manage().window().maximize();
                        break;
                    case BROWSER_NAME_FIREFOX:
                        System.setProperty("webdriver.gecko.driver", basePath + "geckodriver");
                        driver = new FirefoxDriver();
                        driver.manage().window().maximize();
                        break;
                    case BROWSER_NAME_IE:
                    case BROWSER_NAME_EDGE:
                    default:
                        throw new IllegalArgumentException(String.format("browser %s not supported for linux...", browserName));
                }
                break;
            case OS_TYPE_WINDOWS:
            default:
                switch (browserName) {
                    case BROWSER_NAME_CHROME:
                        System.setProperty("webdriver.chrome.driver", basePath + "chromedriver.exe");
                        driver = new ChromeDriver();
                        driver.manage().window().maximize();
                        break;
                    case BROWSER_NAME_FIREFOX:
                        System.setProperty("webdriver.gecko.driver", basePath + "geckodriver.exe");
                        driver = new FirefoxDriver();
                        driver.manage().window().maximize();
                        break;
                    case BROWSER_NAME_IE:
                        System.setProperty("webdriver.ie.driver", basePath + "IEDriverServer.exe");
                        driver = new InternetExplorerDriver();
                        driver.manage().window().maximize();
                        break;
                    case BROWSER_NAME_EDGE:
                        System.setProperty("webdriver.edge.driver", basePath + "msedgedriver3.exe");
                        driver = new EdgeDriver();
                        driver.manage().window().maximize();
                        break;
                    default:
                        break;
                }
        }

        //Timeout to make sure landingpage is fully loaded
        driver.manage()
                .timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driver;
    }

    public void getScreenshot(String testName) throws IOException {

        final String screenShotPath = prop.getProperty("screenShotPath");
        final File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(src, new File(screenShotPath + testName + "screenshot.png"));
    }
}
