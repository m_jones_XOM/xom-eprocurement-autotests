package com.xommaterials.eprocurment.autotest.skeleton;

import org.testng.annotations.DataProvider;

public class DataProviderClass {

    @DataProvider(name = "data-provider")
    public static Object[][] dataProviderMethod(){

        //return new Object[][]{{"data one"},{"data two"}};

        Object[][] data = new Object[2][2];

        data[0][0] = "email@spasskopf.de";
        data[0][1] = "12345678";
        data[1][0] = "nocheineemail@quatschkopf.de";
        data[1][1] = "hajkdfhlaKJHFJKa";

        return data;

    }
}
