package com.xommaterials.eprocurment.autotest.pageobejct;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Landingpage {

    public WebDriver driver;

    private final By inputName = By.id("edit_user");
    private final By inputPW = By.id("edit_password");
    private final By submitBtn = By.id("login_button");
    private final By forgotPW = By.xpath("//a[text()='Passwort vergessen']");
    private final By regBuyer = By.xpath("//a[text()='Als Käufer registrieren']");
    private final By lblLogin = By.xpath("//h1[text()='Login']");
    private final By errorMsg = By.xpath("//*[@id=\"login-form\"]/div[1]");

    private final By lblSTart = By.xpath("//h1[text()='Bedarfsübersicht']");

    public Landingpage(WebDriver driver){
        this.driver = driver;
    }

    public WebElement getInputName(){
        return driver.findElement(inputName);
    }

    public WebElement getInputPW(){
        return driver.findElement(inputPW);
    }

    public WebElement getSubmitBtn(){
        return driver.findElement(submitBtn);
    }

    public WebElement getForgotPw(){
        return driver.findElement(forgotPW);
    }

    public WebElement getRegBuyer(){
        return driver.findElement(regBuyer);
    }

    public WebElement getPageTitle(){
        return driver.findElement(lblLogin);
    }

    public WebElement getErrorMsg(){
        return driver.findElement(errorMsg);
    }

}
