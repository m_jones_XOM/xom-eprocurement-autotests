package com.xommaterials.eprocurment.autotest.pageobejct;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MainView {

    public WebDriver driver;

    private final By btnLogout = By.cssSelector(".btn--menu > span:nth-child(2)");
    private final By lblCopyright = By.xpath("//h6[text()='© 2019 XOM Materials GmbH']");
    private final By lblSTart = By.xpath("//h1[text()='Bedarfsübersicht']");

    public MainView(WebDriver driver){
        this.driver = driver;
    }

    public WebElement getLogoutBtn(){
        return driver.findElement(btnLogout);
    }

    public WebElement getCopyright(){
        return driver.findElement(lblCopyright);
    }
}
